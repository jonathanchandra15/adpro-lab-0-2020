package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
import java.util.List;

public class ChainSpell implements Spell {
    private List<Spell> spellList;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spellList = spells;
    }
    // TODO: Complete Me

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        for(Spell spell : spellList) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        int panjang = spellList.size();
        for (int i = panjang - 1; i >= 0; i--) {
            spellList.get(i).cast();
        }
    }
}
