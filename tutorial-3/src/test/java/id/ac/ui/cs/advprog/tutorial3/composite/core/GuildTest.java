package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;
    private Member bawahan;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Naruto", "Master");
        guild = new Guild(guildMaster);
        bawahan = new OrdinaryMember("Sakura", "Bucin");
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        guild.addMember(guildMaster, bawahan);
        assertEquals(1, guildMaster.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        guild.addMember(guildMaster, bawahan);
        assertEquals(1, guildMaster.getChildMembers().size());
        guild.removeMember(guildMaster, bawahan);
        assertEquals(0, guildMaster.getChildMembers().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Sasuke", "Uchiha");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        guild.addMember(guildMaster, bawahan);
        assertEquals(bawahan, guild.getMember("Sakura", "Bucin"));
        assertEquals(null, guild.getMember("Sa", "Bucin"));
    }
}
