package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Aqua", "Goddess");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        String name = member.getName();
        assertEquals("Aqua", name);
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        String role = member.getRole();
        assertEquals("Goddess", role);
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        member.addChildMember(new OrdinaryMember("Adi", "Bebas"));
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member bawahan = new OrdinaryMember("Adi", "Bebas");
        member.addChildMember(bawahan);
        assertEquals(1, member.getChildMembers().size());
        member.removeChildMember(bawahan);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        member.addChildMember(new OrdinaryMember("Anto", "Bucin"));
        member.addChildMember(new OrdinaryMember("Anti", "Bucin"));
        member.addChildMember(new OrdinaryMember("Ante", "Bucin"));
        member.addChildMember(new OrdinaryMember("Anta", "Bucin"));
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member master = new PremiumMember("Saya", "Master");
        master.addChildMember(new OrdinaryMember("Anto", "Bucin"));
        master.addChildMember(new OrdinaryMember("Anti", "Bucin"));
        master.addChildMember(new OrdinaryMember("Ante", "Bucin"));
        master.addChildMember(new OrdinaryMember("Anta", "Bucin"));
        assertEquals(4, master.getChildMembers().size());
    }
}
