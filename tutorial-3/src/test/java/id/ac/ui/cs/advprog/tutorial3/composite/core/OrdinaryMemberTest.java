package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Asuna", "Waifu");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        String name = member.getName();
        assertEquals("Asuna", name);
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        String role = member.getRole();
        assertEquals("Waifu", role);
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        //TODO: Complete me
        Member bawahan = new OrdinaryMember("Anto", "Bucin");
        member.addChildMember(bawahan);
        assertEquals(0, member.getChildMembers().size());
        member.removeChildMember(bawahan);
        assertEquals(0, member.getChildMembers().size());
    }
}
