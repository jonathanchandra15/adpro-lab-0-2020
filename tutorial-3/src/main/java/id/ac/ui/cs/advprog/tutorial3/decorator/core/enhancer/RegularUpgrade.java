package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;
    int value;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
        this.value = new Random().nextInt(5) + 1;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        if(this.weapon != null) return this.value + this.weapon.getWeaponValue();
        else return this.value;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Regular " + this.weapon.getDescription();
    }
}
