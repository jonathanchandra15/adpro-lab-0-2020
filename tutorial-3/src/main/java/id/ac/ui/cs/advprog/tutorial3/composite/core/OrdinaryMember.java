package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
        //TODO: Complete me
    private String nama;
    private String role;
    private List<Member> listMember = new ArrayList <Member>();

    public OrdinaryMember(String nama, String role) {
        this.nama = nama;
        this.role = role;
    }

    @Override
    public String getName() {
        return this.nama;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {
    }

    @Override
    public void removeChildMember(Member member) {
    }

    @Override
    public List<Member> getChildMembers() {
        return this.listMember;
    }
}
