package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.List;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
                //ToDo: Complete Me
                this.guild.add(this);
        }

        //ToDo: Complete Me

        @Override
        public void update() {
                String type = this.guild.getQuestType();
                List<Quest> questList = getQuests();
                if(type.equals("D") || type.equals("E")) {
                        questList.add(this.guild.getQuest());
                }
        }
}
